from selenium.webdriver.common.by import By
from fixtures.testarena.login import browser
from selenium.webdriver.support.wait import WebDriverWait
from pages.add_project_page import AddProjectPage
from selenium.webdriver.support import expected_conditions as EC
from fixtures.testarena.data_string import string_data_random


class ProjectView:
    left_menu_project = (By.CSS_SELECTOR, ".icon_puzzle_alt icon-20")
    test_selector = (By.CSS_SELECTOR, ".article_in_content")
    project_page_button = (By.CSS_SELECTOR, '.icon_tools')
    project_page_add_new_button = (By.CSS_SELECTOR, '.button_link_li:first-child')
    search_field = (By.ID, 'search')
    search_button = (By.ID, 'j_searchButton')
    first_table_row = (By.CSS_SELECTOR, "tbody tr:nth-child(1)")
    first_table_row_prefix = (By.CSS_SELECTOR, "tbody tr:nth-child(1) .t_number")

    def __init__(self, browser, timeout=10):
        self.browser = browser
        self.wait = WebDriverWait(browser, 10)

    def go_to_add_project_view_page(self):
        self.wait.until(EC.presence_of_element_located(self.left_menu_project))
        self.browser.find_element(*self.left_menu_project).click()
        return AddProjectPage(self.browser)

    def find_element_on_project_view_page(self):
        return self.browser.find_element(*self.test_selector).text

    def go_to_add_project_page(self):
        self.wait.until(EC.presence_of_element_located(self.project_page_add_new_button))
        self.browser.find_element(*self.project_page_add_new_button).click()
        return AddProjectPage(self.browser)

    def go_to_project_search(self):
        self.browser.find_element(*self.project_page_button).click()
        return self

    def set_value_in_search_field(self, prefix):
        self.browser.find_element(*self.search_field).send_keys(prefix)
        self.browser.find_element(*self.search_field).click()
        return self

    def find_element_row(self):
        self.browser.find_element(*self.first_table_row_prefix).text
        return self.browser.find_element(*self.first_table_row_prefix).text

    def is_loaded(self):
        try:
            WebDriverWait(self.browser, 10).until(
                EC.element_to_be_clickable(*self.project_view_page_button_check)
            )
            return True
        except:
            return False
