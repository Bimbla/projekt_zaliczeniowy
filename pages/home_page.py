from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from pages.project_page import ProjectPage
from pages.messages_page import MessagesPage
from pages.project_view import ProjectView
from fixtures.testarena.login import browser
from selenium.webdriver.support.wait import WebDriverWait


class HomePage:
    user_email = (By.CSS_SELECTOR, ".user-info small")
    mail_icon = (By.CLASS_NAME, 'icon_mail')
    project_page_button = (By.CSS_SELECTOR, '.icon_tools')
    left_menu_project_icon = (By.CSS_SELECTOR, 'span.icon_puzzle_alt.icon-20')

    # def __init__(self, browser):
    #     self.browser = browser

    def __init__(self, browser, timeout=10):
        self.browser = browser
        self.wait = WebDriverWait(browser, timeout)

    def get_current_user_email(self):
        return self.browser.find_element(*self.user_email).text

    def click_mail_icon(self):
        self.browser.find_element(*self.mail_icon).click()
        return MessagesPage(self.browser)

    def go_to_project_page(self):
        self.browser.find_element(*self.project_page_button).click()
        return ProjectPage(self.browser)

    def clik_project_icon_from_left_menu(self):
        self.wait.until(EC.presence_of_element_located(self.left_menu_project_icon))
        self.browser.find_element(*self.left_menu_project_icon).click()
        return ProjectView(self.browser)