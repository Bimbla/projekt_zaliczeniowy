from selenium.common import TimeoutException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from fixtures.testarena.login import browser


class AddProjectPage:
    project_name = (By.ID, 'name')
    project_prefix = (By.ID, 'prefix')
    project_description = (By.ID, 'description')
    project_save = (By.ID, 'save')
    content_title = (By.CSS_SELECTOR, '.content_title')
    search_field = (By.ID, 'search')
    search_button = (By.ID, 'j_searchButton')
    first_table_row = (By.CSS_SELECTOR, "tbody tr:nth-child(1)")
    first_table_row_prefix = (By.CSS_SELECTOR, "tbody tr:nth-child(1) .t_number")

    def __init__(self, browser):
        self.browser = browser

    def put_project_data(self, name, prefix, description):
        WebDriverWait(self.browser, 10).until(
            EC.presence_of_element_located(self.project_name)
        ).send_keys(name)
        WebDriverWait(self.browser, 10).until(
            EC.presence_of_element_located(self.project_prefix)
        ).send_keys(prefix)
        WebDriverWait(self.browser, 10).until(
            EC.presence_of_element_located(self.project_description)
        ).send_keys(description)
        self.browser.find_element(*self.project_save).click()

        def go_to_project_page(self):
            self.browser.find_element(*self.project_page_button).click()
            return (self.browser)


    def get_search_field(self):
        return '#search-input'

    def get_search_button(self):
        return '#search-button'

    def set_value_in_search_field(self, prefix):
        self.browser.find_element(*self.search_field).send_keys(prefix)
        self.browser.find_element(*self.search_button).click()
        return self

    def check_returned_prefix(self,first_table_row_prefix):
        self.browser.find_element(self, first_table_row_prefix).text
        return self

    def find_element_on_add_project_page(self):
        return self.browser.find_element(*self.project_save)

    def is_loaded(self):
        try:
            WebDriverWait(self.browser, 10).until(
                EC.presence_of_element_located(self.content_title)
            )
            return True
        except:
            return False
