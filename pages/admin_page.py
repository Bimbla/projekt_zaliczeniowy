
from selenium.webdriver.common.by import By

class AdminPage:
    administration_link = (By.CSS_SELECTOR, '#head-top .header_admin a[title="Administracja"]')
    administration_bottom= (By.CSS_SELECTOR, ".header_admin > a .icon_tools")
    bottom_add_project = (By.CSS_SELECTOR, "a.button_link")
    def __init__(self, browser):
        self.browser = browser

    def click_administration(self):
        self.browser.find_element(*self.administration_bottom).click()
        return AdminPage(self.browser)
