import sys
import time

from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from fixtures.testarena.login import browser
from pages.add_project_page import AddProjectPage
from fixtures.testarena.data_string import string_data_random


class ProjectPage:
    project_page_add_new_button = (By.CSS_SELECTOR, '.button_link_li:first-child')
    project_page_button = (By.CSS_SELECTOR, '.icon_tools')
    search_field = (By.ID, 'search')
    search_button = (By.ID, 'j_searchButton')
    first_table_row = (By.CSS_SELECTOR, "tbody tr:nth-child(1)")
    first_table_row_prefix = (By.CSS_SELECTOR, "tbody tr:nth-child(1) .t_number")

    def __init__(self, browser, timeout=10):
        self.browser = browser
        self.wait = WebDriverWait(browser, 10)

    def go_to_add_project_page(self):
        self.wait.until(EC.presence_of_element_located(self.project_page_add_new_button))
        self.browser.find_element(*self.project_page_add_new_button).click()
        return AddProjectPage(self.browser)

    def find_element_on_home_page(self):
        return self.browser.find_element(*self.project_page_add_new_button).text

    def find_element_in_search_resalt(self):
        self.wait.until(EC.presence_of_element_located(self.first_table_row))
        return self.browser.find_element(*self.first_table_row_prefix).text

    def get_prefix(self):
        return self.prefix

    def get_search_field(self):
        return self.search_field

    def get_search_button(self):
        return self.search_button

    def search_by_prefix(self, prefix):
        search_field = self.get_search_field()
        search_button = self.get_search_button()

        search_input = self.browser.find_element(*search_field)
        search_input.clear()
        search_input.send_keys(prefix)
        self.browser.find_element(*search_button).click()
        time.sleep(100)
        # self.wait.until(EC.presence_of_element_located(self.first_table_row))
        results = self.browser.find_elements(By.CSS_SELECTOR, "tbody tr:nth-child(1)")
        return results

    # def is_loaded(self, wait_for_element, timeout=10):
    #     return wait_for_element(self.browser,self.project_page_add_new_button,timeout)

    def is_loaded(self):
        try:
            WebDriverWait(self.browser, 10).until(
                EC.presence_of_element_located(self.project_page_add_new_button)
            )
            return True
        except:
            return False
