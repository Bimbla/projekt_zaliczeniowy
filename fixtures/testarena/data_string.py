import random
import string
import pytest

@pytest.fixture(scope="session")
def string_data_random():
    return {}

@pytest.fixture
def store_string_data(string_data_random):
    def _store(key, value):
        string_data_random[key] = value
    return _store



@pytest.fixture(scope="session")
def generate_random_string():
    def _generate_random_string(length=10):
        letters = string.ascii_letters
        return ''.join(random.choice(letters) for i in range(length))

    return _generate_random_string