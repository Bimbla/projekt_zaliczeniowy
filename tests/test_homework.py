from pages.admin_page import AdminPage
from pages.home_page import HomePage
from pages.add_project_page import AddProjectPage
from fixtures.testarena.login import browser
from fixtures.testarena.data_string import string_data_random
from fixtures.testarena.data_string import generate_random_string
from fixtures.testarena.data_string import store_string_data
from fixtures.chrome import chrome_browser

administrator_email = 'administrator@testarena.pl'


def test_successful_login(browser):
    home_page = HomePage(browser)
    user_email = home_page.get_current_user_email()
    assert administrator_email == user_email


def test_open_admin_panel(browser):
    home_page = HomePage(browser)
    home_page.click_mail_icon()


def test_click_administration(browser):
    home_page = AdminPage(browser)
    home_page.click_administration()


def test_navigation_to_project_page(browser):
    home_page = HomePage(browser)
    project_page = home_page.go_to_project_page()
    assert project_page.is_loaded()


def test_go_to_add_project(browser):
    home_page = HomePage(browser)
    project_page = home_page.go_to_project_page()
    view_project_page = project_page.go_to_add_project_page()
    assert view_project_page.is_loaded()


def test_put_project_data(browser, generate_random_string, string_data_random):
    home_page = HomePage(browser)
    project_page = home_page.go_to_project_page()
    project_page.go_to_add_project_page()
    add_project_page = AddProjectPage(browser)
    name = generate_random_string(10)
    prefix = generate_random_string(6)
    description = "długi opis na 2000 znaków"
    add_project_page.put_project_data(name, prefix, description)
    string_data_random['name'] = name
    string_data_random['prefix'] = prefix
    string_data_random['description'] = description
    assert project_page.is_loaded()


def test_project_is_added(browser, string_data_random):
    home_page = HomePage(browser)
    project_view = home_page.clik_project_icon_from_left_menu()
    assert project_view.find_element_on_project_view_page()
    search_panel = project_view.go_to_project_search()
    real_prefix = string_data_random.get('prefix')
    print(real_prefix)
    search_panel.set_value_in_search_field(real_prefix)
    first_table_row_prefix = search_panel.find_element_row()
    assert real_prefix == first_table_row_prefix
